# coding: utf-8
from lesson05_layer_naive import *

apple_price = 100
apple_num = 2
orange_price = 150
orange_num = 3
tax = 1.1

# layer
mul_apple_layer = MulLayer()
mul_orange_layer = MulLayer()
add_apple_orange_layer = AddLayer()
mul_tax_layer = MulLayer()

# forward
apple_cost = mul_apple_layer.forward(apple_price, apple_num)
orange_cost = mul_orange_layer.forward(orange_price, orange_num)
apple_orange_cost = add_apple_orange_layer.forward(apple_cost, orange_cost)
total_cost = mul_tax_layer.forward(apple_orange_cost, tax)

print("total_cost => ", total_cost)

# backward
dcost = 1
dapple_orange_cost, dtax = mul_tax_layer.backward(dcost)
dapple_cost, dorange_cost = add_apple_orange_layer.backward(dapple_orange_cost)
dapple_price, dapple_num = mul_apple_layer.backward(dapple_cost)
dorange_price, dorange_num = mul_orange_layer.backward(dorange_cost)

print(f"dapple_price {dapple_price }, dapple_num {dapple_num}, dorange_price {dorange_price }, dorange_num {dorange_num}, dtax {dtax}")

# total_cost =>  715.0000000000001
# dapple_price 2.2, dapple_num 110.00000000000001, dorange_price 3.3000000000000003, dorange_num 165.0, dtax 650
