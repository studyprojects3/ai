# coding: utf-8
from lesson05_layer_naive import MulLayer

apple_price = 100
apple_num = 2
tax = 1.1

# layer
mul_apple_layer = MulLayer()
mul_tax_layer = MulLayer()

# forward
apple_cost = mul_apple_layer.forward(apple_price, apple_num)
total_cost = mul_tax_layer.forward(apple_cost, tax)

print("total_cost => ", total_cost)

# backward
dcost = 1
dapple_cost, dtax = mul_tax_layer.backward(dcost)
dapple_price, dapple_num = mul_apple_layer.backward(dapple_cost)

print(f"dapple_price {dapple_price }, dapple_num {dapple_num}, dtax {dtax}")

# total_cost =>  220.00000000000003
# dapple_price 2.2, dapple_num 110.00000000000001, dtax 200
