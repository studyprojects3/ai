#-*- coding: utf-8 -*-
# lesson 03 mnist show

import sys, os
sys.path.append(os.pardir)
import numpy as np
from dataset.mnist import load_mnist
from PIL import Image
import matplotlib.pyplot as plt

def img_show(img):
    pil_img = Image.fromarray(np.uint8(img))
    pil_img.show()

## load_mnist 的返回值:
## (训练图像 ,训练标签)，(测试图像，测试标签)
(x_train, t_train), (x_test, t_test) = load_mnist(flatten=True, normalize=False)

print("x_train.shape => ", x_train.shape)
print("t_train.shape => ", t_train.shape)
print("x_test.shape => ", x_test.shape)
print("t_test.shape => ", t_test.shape)

img = x_train[0]
label = t_train[0]
print("t_train[0] => ", label)
print("img.shape  => ", img.shape)
img = img.reshape(28, 28)
print("img.shape  => ", img.shape)
img_show(img)
