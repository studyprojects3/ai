#-*- coding: utf-8 -*-
# lesson 03 softmax function

import numpy as np

def softmax_simple(a):
    exp_a = np.exp(a)
    sum_exp_a = np.sum(exp_a)
    y = exp_a / sum_exp_a
    return y

def softmax(a):
    c = np.max(a)
    exp_a = np.exp(a-c)
    sum_exp_a = np.sum(exp_a)
    y = exp_a / sum_exp_a
    return y

a = np.array([0.3, 2.9, 4.0])
exp_a = np.exp(a)
print("exp(a) => ", exp_a)
sum_exp_a = np.sum(exp_a)
print("sum_exp_a => ", sum_exp_a)
y = exp_a / sum_exp_a
print("y => ", y)

a = np.array([1010, 1000, 990])
y0 = softmax_simple(a)
print("y0 => ", y0)
y1 = softmax(a)
print("y1 => ", y1)
# y0 =>  [nan nan nan]
# y1 =>  [9.99954600e-01 4.53978686e-05 2.06106005e-09]
