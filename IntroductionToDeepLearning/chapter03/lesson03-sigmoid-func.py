#-*- coding: utf-8 -*-
# lesson 03 step function

import numpy as np
import matplotlib.pylab as plt

def step_function_simple(x):
    if x>0:
        return 1
    else:
        return 0

def step_function(x):
    y = x>0
    # numpy 1.20.0 将 np.int 替换为 np.int_
    return y.astype(np.int_)

x = np.array([-1.0, 1.0, 2.0])
print("x => ", x)
y = x>0
print("y => ", y)
z = y.astype(np.int_)
print("z => ", z)

x = np.arange(-5.0, 5.0, 0.1)
y = step_function(x)
plt.plot(x,y)
plt.ylim(-0.1, 1.1)
plt.show()

