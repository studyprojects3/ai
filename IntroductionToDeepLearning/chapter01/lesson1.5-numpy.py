#-*- coding: utf-8 -*-
# lesson 1.5 numpy

import numpy as np

# -------------------------------
# 生成数组
print("# ------------------------")
x = np.array([1.0, 2.0, 3.0])
print(x)
print(type(x))  # <class 'numpy.ndarray'>

# -------------------------------
# 算术运算
print("# ------------------------")
x = np.array([1.0, 2.0, 3.0])
y = np.array([2.0, 4.0, 6.0])
print(x + y)    # [3. 6. 9.]
print(x - y)    # [-1. -2. -3.]
print(x * y)    # [ 2.  8. 18.]
print(x / y)    # [0.5 0.5 0.5]

print(x / 2)    # [0.5 1.  1.5]

# -------------------------------
# NumPy的N维数组
# NumPy 的矩阵（通常是指二维数组）在内存中的存储顺序默认是按照 C-style 的行主序来存储的，这意味着矩阵中的元素是按照从左到右，再从上到下的顺序存储的。
print("# ------------------------")
A = np.array([[1, 2], [3, 4]])
print(A)
#[[1 2]
# [3 4]]
print(A.shape) # (2, 2)
print(A.dtype) # int32

B = np.array([[3, 0],[0, 6]])
print(A+B)
#[[ 4  2]
# [ 3 10]]

print(A*10)
#[[10 20]
# [30 40]]

# -------------------------------
# 广播
print("# ------------------------")
A = np.array([[1, 2], [3, 4]])
B = np.array([10, 20])
print(A * B)
#[[10 40]
# [30 80]]

# -------------------------------
# 访问元素
print("# ------------------------")
X = np.array([[51, 55], [14, 19], [0, 4]])
print(X)
#[[51 55]
# [14 19]
# [ 0 4]]
print(X[0])        # 第0行 [51, 55]
print(X[0][1])     # (0, 1) 的元素 55

for row in X:
	print(row)
# [51 55]
# [14 19]
# [0 4]

X = X.flatten() # 将X转换为一维数组
print(X)        # [51 55 14 19 0 4]
X1 = X[np.array([0, 2, 4])] # 获取索引为0、2、4的元素
print(X1)                   # array([51, 14, 0])

X2 = X > 15
print(X2)       # [ True  True False  True False False]
X3 = X[X>15]
print(X3)       # [51 55 19]

# -------------------------------
# 维度 和Shape
print("# ------------------------")
A = np.array([1,2,3,4])
print("A          => ", A)
print("np.ndim(A) => ", np.ndim(A))
print("A.shape    => ", A.shape)     # shape 为 (4,)  可理解为4行1列，也可理解为1行4列
print("A.shape[0] => ", A.shape[0])  # shape 为 4
B = np.array([[1,2],[3,4],[5,6]])
print("B          => \n", B)
print("np.ndim(B) => ", np.ndim(B))
print("B.shape    => ", B.shape)

# -------------------------------
# 矩阵乘法
print("# ------------------------")
A = np.array([1,2,3,4])
C = np.dot(A, A)
print("C          => ", C)
A = np.array([[1,2,3],[4,5,6]])
print("A          => \n", A)
print("A.shape    => ", A.shape)
B = np.array([[1,2],[3,4],[5,6]])
print("B          => \n", B)
print("B.shape    => ", B.shape)
C = np.dot(A, B)
print("C          => \n", C)

# -------------------------------
# np.argmax
print("# ------------------------")
x = np.array([
    [0.1,0.8,0.1],
    [0.3,0.1,0.6],
    [0.2,0.5,0.3],
    [0.8,0.1,0.1]])
y = np.argmax(x, axis=1)   # 指定从 axis=1(行方向) 选最大的
z = np.argmax(x, axis=0)   # 指定从 axis=0(列方向) 选最大的
print("y => ", y)
print("z => ", z)

# -------------------------------
# np.sum
print("# ------------------------")
x = np.array([1,2,1,0])
y = np.array([1,2,0,0])
print("x==y         => ", x==y)
print("np.sum(x==y) => ", np.sum(x==y))
x = np.array([[1,2,1],[4,5,6]])
print("np.sum(x**2) => ", np.sum(x**2, axis=1))  # 指定指定对 axis=1(行方向) 求和
# x==y         =>  [ True  True False  True]
# np.sum(x==y) =>  3
# np.sum(x**2) =>  [ 6 77]

# -------------------------------
# np.array 筛选数据
print("# ------------------------")
x = np.array([
    [1, 2, 3],
    [4, 5, 6],
    ])
t = np.array([1, 2])
print("x.shape        => ", x.shape)
batch_size = x.shape[0]
print("x.shape[0]     => ", x.shape[0])
group_idx = np.arange(batch_size)
print("group_idx      => ", group_idx)
selected_elems = x[group_idx, t]              # t=[1,2] 表示第0组数据选择第1个，第1组数据选择第2个
print("selected_elems => ", selected_elems)
#x.shape        =>  (2, 3)
#x.shape[0]     =>  2
#group_idx      =>  [0 1]
#selected_elems =>  [2 6]

# -------------------------------
# np.random.choice
print("# ------------------------")
x_train = np.array([
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
    [10, 11, 12],
    [13, 14, 15],
    [16, 17, 18],
    [19, 20, 21],
    ])
train_size = x_train.shape[0]
print("train_size => ", train_size)
print("x_train    => \n", x_train)
batch_size = 4
batch_mask = np.random.choice(train_size, batch_size)
print("batch_mask => ", batch_mask)
x_batch = x_train[batch_mask]
print("x_batch    => \n", x_batch)

# -------------------------------
# 遍历元素
print("# ------------------------")
X = np.array([
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
    ])
for idx, x in enumerate(X):
    print("idx => ", idx)
    print("x   => ", x)

# -------------------------------
# np.meshgrid
print("# ------------------------")
x0 = np.arange(-1, 1.5, 0.5)
x1 = np.arange(-1, 1.5, 0.5)

X, Y = np.meshgrid(x0, x1)
print("X => \n", X)
print("Y => \n", Y)
