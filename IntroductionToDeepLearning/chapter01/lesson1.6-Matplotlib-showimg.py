#-*- coding: utf-8 -*-
# lesson 1.6 Matplotlib

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.image import imread

img = imread("../dataset/lena.png")
plt.imshow(img)

plt.show()
