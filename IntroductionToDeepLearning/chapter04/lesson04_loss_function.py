#-*- coding: utf-8 -*-
# lesson 04 loss function

import sys, os
sys.path.append(os.pardir)
import numpy as np
from dataset.mnist import load_mnist
import matplotlib.pylab as plt

y = np.array([0.1, 0.8, 0.1])
t = np.array([0, 1, 0])
mean_squred_error = 0.5 * np.sum((y-t)**2)
print("mean_squred_error => ", mean_squred_error)
# mean_squred_error =>  0.029999999999999992

def cross_entropy_error_simple(y, t):
    # np.log(0) 为负无穷大，添加一个微小值以防止负无穷大
    delta = 1e-7
    return -np.sum(t * np.log(y + delta))

def cross_entropy_error_batch(y, t):
    if y.ndim == 1:
        t = t.reshape(1, t.size)
        y = y.reshape(1, t.size)

    batch_size = y.shape[0]
    # np.log(0) 为负无穷大，添加一个微小值以防止负无穷大
    delta = 1e-7
    # t 中数据以非one-hot 表示时，才可以使用 y[np.arange(batch_size), t]
    return -np.sum(np.log(y[np.arange(batch_size), t] + delta))

y = np.array([0.1, 0.8, 0.1])
t = np.array([0, 1, 0])
cross_entropy_err = cross_entropy_error_simple(y, t)
print("cross_entropy_err => ", cross_entropy_err)
# cross_entropy_err =>  0.22314342631421757

## load_mnist 的返回值:
## (训练图像 ,训练标签)，(测试图像，测试标签)
(x_train, t_train), (x_test, t_test) = load_mnist(flatten=True, normalize=False, one_hot_label=False)
print("x_train.shape => ", x_train.shape)
print("t_train.shape => ", t_train.shape)
print("x_test.shape => ", x_test.shape)
print("t_test.shape => ", t_test.shape)

img0 = x_train[0]
label0 = t_train[0]   # one_hot_label=False 时，label 为5
img2 = x_train[2]
label2 = t_train[2]
print("t_train[0] => ", label0)
print("img0.shape  => ", img0.shape)
print("t_train[2] => ", label2)
print("img2.shape  => ", img2.shape)




